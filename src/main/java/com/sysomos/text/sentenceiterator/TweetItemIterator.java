package com.sysomos.text.sentenceiterator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sysomos.text.filter.BaseTweetItemFilter;

public class TweetItemIterator extends FileSentenceIterator {

	private static final Logger LOG = LoggerFactory
			.getLogger(TweetItemIterator.class);
	private boolean ignoreRetweets = false;

	public TweetItemIterator(File file) {
		super(file);
	}

	public TweetItemIterator(File file, BaseTweetItemFilter rule) {
		super(file);
	}

	public void setIgnoreRetweets(boolean ignoreRetweets) {
		this.ignoreRetweets = ignoreRetweets;
	}

	public TweetItem nextTweet() {
		String sentence = super.nextSentence();
		while (StringUtils.isEmpty(sentence)) {
			sentence = super.nextSentence();
		}
		try {
			TweetItem tweetItem = new ObjectMapper().readValue(sentence,
					TweetItem.class);
			return tweetItem;
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

	public String nextSentence() {
		if (!hasNext())
			return null;
		TweetItem tweetItem = nextTweet();
		while (tweetItem == null && super.hasNext()) {
			tweetItem = nextTweet();
		}
		while (tweetItem != null) {
			String sentence = tweetItem.text;
			if (StringUtils.isEmpty(sentence)
					|| (ignoreRetweets && sentence.indexOf("@RT") >= 0)) {
				tweetItem = nextTweet();
				continue;
			}
			return sentence;
		}
		return tweetItem == null ? null : tweetItem.text;
	}
}
