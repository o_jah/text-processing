package com.sysomos.text.ngrams;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class NgramBuilder {

	private static int TOKEN_SIZE = 4;

	public static List<String> build(String str) {
		if (StringUtils.isEmpty(str))
			return null;

		List<String> strList = new ArrayList<String>();
		for (int i = 0; i < str.length(); i += TOKEN_SIZE) {
			if (i + TOKEN_SIZE < str.length()) {
				String sub = str.substring(i, i + TOKEN_SIZE);
				if (!StringUtils.isEmpty(sub)) {
					strList.add(sub);
				}
			} else {
				String sub = str.substring(i);
				if (!StringUtils.isEmpty(sub)) {
					strList.add(sub);
				}
			}
		}
		String tokens[] = strList.toArray(new String[strList.size()]);

		List<String> ngramList = new ArrayList<String>();
		int ngrams = tokens.length;
		while (ngrams >= 1) {
			ngramList.addAll(concat(tokens, ngrams--));
		}
		return ngramList;
	}

	public static List<String> build(String str, int ngrams) {
		if (StringUtils.isEmpty(str))
			return null;

		List<String> strList = new ArrayList<String>();
		for (int i = 0; i < str.length(); i += TOKEN_SIZE) {
			if (i + TOKEN_SIZE < str.length()) {
				String sub = str.substring(i, i + TOKEN_SIZE);
				if (!StringUtils.isEmpty(sub)) {
					strList.add(sub);
				}
			} else {
				String sub = str.substring(i);
				if (!StringUtils.isEmpty(sub)) {
					strList.add(sub);
				}
			}
		}
		String tokens[] = strList.toArray(new String[strList.size()]);

		List<String> ngramList = new ArrayList<String>();
		ngramList.addAll(concat(tokens, ngrams));
		return ngramList;
	}

	private static List<String> concat(String tokens[], int ngrams) {
		List<String> ngramList = new ArrayList<String>();
		if (tokens == null)
			return ngramList;
		for (int i = 0; i < tokens.length; i++) {
			String ngramStr = "";
			for (int j = i; j < i + ngrams; j++) {
				if (j < tokens.length && !StringUtils.isEmpty(tokens[j])) {
					ngramStr += tokens[j] + " ";
				}
			}
			if (!StringUtils.isEmpty(ngramStr)) {
				ngramList.add(ngramStr.trim().intern());
			}
		}
		return ngramList;
	}
}
