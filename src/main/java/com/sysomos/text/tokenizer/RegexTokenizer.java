package com.sysomos.text.tokenizer;

import java.util.List;

public class RegexTokenizer implements Tokenizer {

	private TokenPreProcess tokenPreProcess;

	public RegexTokenizer(String toTokenize, String regex) {
		
	}

	@Override
	public boolean hasMoreTokens() {
		return false;
	}

	@Override
	public int countTokens() {
		return 0;
	}

	@Override
	public String nextToken() {
		return null;
	}

	@Override
	public List<String> getTokens() {
		return null;
	}

	@Override
	public void setTokenPreProcessor(TokenPreProcess tokenPreProcessor) {
		this.tokenPreProcess = tokenPreProcessor;
	}

}
