package com.sysomos.text.worditerator;

import com.sysomos.text.tokenizer.factory.TokenizerFactory;

public class LineWordIterator extends BaseWordIterator {

	public LineWordIterator(String sentence) {
		super(sentence);
	}

	public LineWordIterator(String sentence, boolean skipStopWords) {
		super(sentence, skipStopWords);
	}

	public LineWordIterator(String sentence, String delimiter) {
		super(sentence, delimiter);
	}

	public LineWordIterator(String sentence,
			TokenizerFactory tokenizerFactory) {
		this(sentence, tokenizerFactory, false);
	}

	public LineWordIterator(String sentence, TokenizerFactory tokenizerFactory,
			boolean skipStopWords) {
		super(sentence, tokenizerFactory, false);
	}
}
