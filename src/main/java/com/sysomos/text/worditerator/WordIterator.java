package com.sysomos.text.worditerator;

import com.sysomos.text.sentenceiterator.SentenceIterator;

public interface WordIterator extends SentenceIterator {

	/**
	 * Gets the next word or null if there's nothing left (Do yourself a favor
	 * and check hasNext() )
	 * 
	 * @return the next word in the iterator
	 */
	String nextWord();

	String nextSentence();

	/**
	 * Same idea as {@link java.util.Iterator}
	 * 
	 * @return whether there's anymore sentences left
	 */
	boolean hasNext();

	/**
	 * Resets the iterator to the beginning
	 */
	void reset();

	/**
	 * Allows for any finishing (closing of input streams or the like)
	 */
	void finish();
}
