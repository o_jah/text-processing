package com.sysomos.text.worditerator;

import com.sysomos.text.string.utils.StringUtils;
import com.sysomos.text.tokenizer.Tokenizer;
import com.sysomos.text.tokenizer.factory.DefaultTokenizerFactory;
import com.sysomos.text.tokenizer.factory.RegexTokenizerFactory;
import com.sysomos.text.tokenizer.factory.TokenizerFactory;
import com.sysomos.text.utils.StopWords;

public abstract class BaseWordIterator {

	private final Tokenizer tokenizer;
	private boolean skipStopWords = false;

	public BaseWordIterator(String sentence) {
		this(sentence, new DefaultTokenizerFactory());
	}

	public BaseWordIterator(String sentence, boolean skipStopWords) {
		this(sentence, new DefaultTokenizerFactory(), skipStopWords);
	}

	public BaseWordIterator(String sentence, String delimiter) {
		tokenizer = StringUtils.isEmptyOrBlank(delimiter)
				? new DefaultTokenizerFactory().create(sentence)
				: new RegexTokenizerFactory(delimiter).create(sentence);
		this.skipStopWords = false;
	}

	public BaseWordIterator(String sentence,
			TokenizerFactory tokenizerFactory) {
		this(sentence, tokenizerFactory, false);
	}

	public BaseWordIterator(String sentence, TokenizerFactory tokenizerFactory,
			boolean skipStopWords) {
		tokenizer = tokenizerFactory.create(sentence);
		this.skipStopWords = skipStopWords;
	}

	public boolean hasNext() {
		return tokenizer.hasMoreTokens();
	}

	public String nextWord() {
		String next = tokenizer.nextToken();
		if (StopWords.getStopWords().contains(next) && skipStopWords)
			next = tokenizer.nextToken();
		return next;
	}
}
