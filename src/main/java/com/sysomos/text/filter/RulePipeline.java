package com.sysomos.text.filter;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.text.filter.rule.TextRule;
import com.sysomos.text.filter.rule.TweetItemRule;

public class RulePipeline {

	public List<BaseRule> ruleInstances;

	private static final Logger LOG = LoggerFactory
			.getLogger(RulePipeline.class);

	public RulePipeline(List<Class<? extends BaseRule>> rules) {
		ruleInstances = new ArrayList<BaseRule>();
		if (CollectionUtils.isEmpty(rules))
			return;
		for (Class<? extends BaseRule> clazz : rules) {
			try {
				BaseRule instance = null;
				if (clazz.isAssignableFrom(TextRule.class))
					instance = clazz.getConstructor(TextRule.class)
							.newInstance();
				else if (clazz.isAssignableFrom(TweetItemRule.class))
					instance = clazz.getConstructor(TweetItemRule.class)
							.newInstance();
				if (instance == null)
					continue;
				ruleInstances.add(instance);
			} catch (InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| NoSuchMethodException | SecurityException e) {
				LOG.error(e.getLocalizedMessage(), e);
			}
		}
	}
}
