package com.sysomos.text.filter.rule;

import com.sysomos.base.filter.rule.BaseRule;

public abstract class TextRule extends BaseRule {
	
	public abstract boolean fires(String text);
}
