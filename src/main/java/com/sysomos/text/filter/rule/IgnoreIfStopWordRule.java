package com.sysomos.text.filter.rule;

import java.util.Collection;

import com.sysomos.text.utils.StopWords;

public class IgnoreIfStopWordRule extends TextRule {

	private static final Collection<String> stopWords = StopWords
			.getStopWords();

	@Override
	public boolean fires(String text) {
		return stopWords.contains(text);
	}

}
