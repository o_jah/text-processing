package com.sysomos.text.filter.rule;

import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.text.sentenceiterator.TweetItem;

public abstract class TweetItemRule extends BaseRule {

	public TweetItemRule() {}
	
	public abstract boolean fires(TweetItem item);
}
