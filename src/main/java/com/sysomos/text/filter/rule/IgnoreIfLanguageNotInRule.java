package com.sysomos.text.filter.rule;

import java.io.File;
import java.util.List;

import com.cybozu.labs.langdetect.Detector;
import com.cybozu.labs.langdetect.DetectorFactory;
import com.cybozu.labs.langdetect.LangDetectException;

public class IgnoreIfLanguageNotInRule extends TextRule {

	private Detector detector = null;
	private final List<String> languages;

	public IgnoreIfLanguageNotInRule(List<String> languages) {
		this.languages = languages;
		try {
			File profile = new File(
					IgnoreIfLanguageNotInRule.class.getClassLoader()
							.getResource("langdetect/profiles").getFile());
			DetectorFactory.loadProfile(profile);
			detector = DetectorFactory.create();
		} catch (LangDetectException e) {
			getLogger().warn(e.getLocalizedMessage());
		}

	}

	@Override
	public boolean fires(String text) {
		if (detector == null)
			return false;
		detector.append(text);
		try {
			return !languages.contains(detector.detect());
		} catch (LangDetectException e) {
			getLogger().warn(e.getLocalizedMessage());
			return false;
		}
	}

}
