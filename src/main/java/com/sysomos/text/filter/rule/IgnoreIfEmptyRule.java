package com.sysomos.text.filter.rule;

import org.apache.commons.lang3.StringUtils;

public class IgnoreIfEmptyRule extends TextRule {

	@Override
	public boolean fires(String text) {
		return StringUtils.isEmpty(text);
	}

}
