package com.sysomos.text.filter;

import java.util.ArrayList;
import java.util.List;

import com.sysomos.base.filter.rule.BaseRule;

public class TweetItemFilterFactory {

	public static BaseTweetItemFilter create(RulePipeline pipeline) {
		return pipeline == null ? null : create(pipeline.ruleInstances);
	}

	public static BaseTweetItemFilter create(List<BaseRule> ruleset) {
		return new TweetItemFilter(ruleset);
	}

	public static BaseTweetItemFilter create(BaseRule... rules) {
		List<BaseRule> ruleset = new ArrayList<BaseRule>();
		for (BaseRule instance : rules) {
			if (instance != null)
				ruleset.add(instance);
		}
		return create(ruleset);
	}
}
