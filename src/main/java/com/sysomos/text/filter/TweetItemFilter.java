package com.sysomos.text.filter;

import java.util.List;

import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.text.filter.rule.TextRule;
import com.sysomos.text.filter.rule.TweetItemRule;
import com.sysomos.text.sentenceiterator.TweetItem;

public class TweetItemFilter extends BaseTweetItemFilter {

	TweetItemFilter(List<BaseRule> ruleInstances) {
		super(ruleInstances);
	}

	@Override
	public boolean exclude(TweetItem item) {
		if (item == null)
			return true;
		for (BaseRule rule : ruleInstances) {
			if (rule != null && rule instanceof TextRule
					&& ((TextRule) rule).fires(item.text))
				return true;
			if (rule != null && rule instanceof TweetItemRule
					&& ((TweetItemRule) rule).fires(item))
				return true;
		}
		return false;
	}

}
