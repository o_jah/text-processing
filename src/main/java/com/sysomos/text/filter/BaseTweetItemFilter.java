package com.sysomos.text.filter;

import java.util.List;

import com.sysomos.base.filter.rule.BaseRule;
import com.sysomos.text.sentenceiterator.TweetItem;

public abstract class BaseTweetItemFilter {

	protected List<BaseRule> ruleInstances;

	BaseTweetItemFilter(List<BaseRule> ruleInstances) {
		this.ruleInstances = ruleInstances;
	}

	public abstract boolean exclude(TweetItem item);
}
