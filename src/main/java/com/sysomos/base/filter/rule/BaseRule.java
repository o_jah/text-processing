package com.sysomos.base.filter.rule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseRule {

	private static final Logger LOG = LoggerFactory.getLogger(BaseRule.class);
	
	protected Logger getLogger() {
		return LOG;
	}
}
