package com.sysomos.text.filter.rule;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class IgnoreIfLanguageNotInRuleTest {

	private IgnoreIfLanguageNotInRule rule;
	private static List<String> languages = new ArrayList<String>();

	static {
		languages.add("ar");
		languages.add("en");
		languages.add("fr");
		languages.add("pt");
		languages.add("sp");
	}

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		rule = new IgnoreIfLanguageNotInRule(languages);
	}

	@Test
	public void firesTest() {
		System.out.println("Fired ?" + rule.fires("Je vais a l'ecole"));
	}
}
