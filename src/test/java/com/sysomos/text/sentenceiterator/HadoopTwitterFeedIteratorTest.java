package com.sysomos.text.sentenceiterator;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class HadoopTwitterFeedIteratorTest {

	private HadoopTwitterFeedIterator iterator;
	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {
		iterator = new HadoopTwitterFeedIterator(27);
	}

	@Test
	public void nextSentenceTest() {
		while (iterator.hasNext()) {
			System.out.println(iterator.nextSentence());
		}
	}

	@After
	public void tearDown() {
		iterator.reset();
		iterator = null;
	}
}
